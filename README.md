# Marching Squares #

This project illustrates simple implementation of the [Marching Squares](https://en.wikipedia.org/wiki/Marching_squares) using C++.

This repository is for anyone who interesting in algorithms and data structure and in applying of these knowledge in the computer science.

Input of this algorithm is an array of integers represents binary image which in turns represents filtered and preprocessed image. Preprocessing was done in order to extract contours of the shapes located in image.

![data_image.png](src/main/assets/input_image.bmp)

There is also a possibility to read data directly from the text file. Data in that file represents described image in the binary format.

First raw is related to width of the image, second one is its height, all other consecutive rows are values of each pixel.

### Who do I talk to? ###

* Repo owner and admin: **Yurii Chernyshov** *chernyshov.yuriy@gmail.com*