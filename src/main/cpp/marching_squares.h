#ifndef DRIVER_ASSISTANT_MARCHINGSQUARES_H
#define DRIVER_ASSISTANT_MARCHINGSQUARES_H

#include <vector>
#include <list>
#include <memory>
#include <climits>
#include "constants.h"
#include "path.h"

using namespace std;

class MarchingSquares {

public:
    MarchingSquares();

    void init(int &_width, int &_height, vector<int> &_data);

    vector<Path> identifyPerimeter();

private:
    vector<int> data;
    int width;
    int height;

    Path identifyPerimeter(int &initialX, int &initialY, vector<vector<int>> &visited);

    int value(int &xCoordinate, int &yCoordinate);

    bool isSet(int &xCoordinate, int &yCoordinate);
};

#endif //DRIVER_ASSISTANT_MARCHINGSQUARES_H
