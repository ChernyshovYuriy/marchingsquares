#include "direction.h"

Direction::Direction() {
    planeX = 0;
    planeY = 0;
    screenX = 0;
    screenY = 0;
    type = TYPE_DEFAULT;
}

void Direction::setNorth() {
    planeX = 0;
    planeY = 1;
    screenX = 0;
    screenY = -1;
    type = TYPE_N;
}

void Direction::setSouth() {
    planeX = 0;
    planeY = -1;
    screenX = 0;
    screenY = 1;
    type = TYPE_S;
}

void Direction::setWest() {
    planeX = -1;
    planeY = 0;
    screenX = -1;
    screenY = 0;
    type = TYPE_W;
}

void Direction::setEast() {
    planeX = 1;
    planeY = 0;
    screenX = 1;
    screenY = 0;
    type = TYPE_E;
}
