#include "marching_squares.h"

MarchingSquares::MarchingSquares() = default;

vector<Path> MarchingSquares::identifyPerimeter() {

    const int size = width * height;
    vector<Path> pathVector;
    vector<vector<int> > visited((unsigned long) (width + 1), vector<int>((unsigned long) (height + 1)));

    for (int i = ZERO; i < size; ++i) {
        if (data[i] != ZERO) {
            int x = i % width;
            int y = i / width;
            Path path = identifyPerimeter(x, y, visited);
            if (path.getDirections().empty()) {
                continue;
            }
            pathVector.push_back(path);
        }
    }

    return pathVector;
}

Path MarchingSquares::identifyPerimeter(int &initialX, int &initialY,
                                        vector<vector<int>> &visited) {
    if (visited[initialX][initialY] == 1) {
        vector<Direction> directions(0);
        return Path(ZERO, ZERO, directions);
    }

    if (initialX < ZERO) initialX = ZERO;
    if (initialX > width) initialX = width;
    if (initialY < ZERO) initialY = ZERO;
    if (initialY > height) initialY = height;

    int initialValue = value(initialX, initialY);
    if (initialValue == ZERO || initialValue == 15) {
        vector<Direction> directions(0);
        return Path(ZERO, ZERO, directions);
    }

    int minX = INT_MAX;
    int maxX = INT_MIN;
    int minY = INT_MAX;
    int maxY = INT_MIN;
    int topPoint[] = {ZERO, ZERO};
    int bottomPoint[] = {ZERO, ZERO};
    int leftPoint[] = {ZERO, ZERO};
    int rightPoint[] = {ZERO, ZERO};

    int x = initialX;
    int y = initialY;
    Direction previous;
    vector<Direction> directions;
    do {
        Direction direction;
        const int valueInt = value(x, y);
        switch (valueInt) {
            case 1:
                direction.setNorth();
                break;
            case 2:
                direction.setEast();
                break;
            case 3:
                direction.setEast();
                break;
            case 4:
                direction.setWest();
                break;
            case 5:
                direction.setNorth();
                break;
            case 6:
                if (previous.type == TYPE_N) {
                    direction.setWest();
                } else {
                    direction.setEast();
                }
                break;
            case 7:
                direction.setEast();
                break;
            case 8:
                direction.setSouth();
                break;
            case 9:
                if (previous.type == TYPE_E) {
                    direction.setNorth();
                } else {
                    direction.setSouth();
                }
                break;
            case 10:
                direction.setSouth();
                break;
            case 11:
                direction.setSouth();
                break;
            case 12:
                direction.setWest();
                break;
            case 13:
                direction.setNorth();
                break;
            case 14:
                direction.setWest();
                break;
            default:
                break;
        }
        directions.push_back(direction);

        visited[x][y] = 1;

        x += direction.screenX;
        y += direction.screenY;

        if (x < minX) {
            minX = x;
            leftPoint[ZERO] = x;
            leftPoint[1] = y;
        }

        if (x > maxX) {
            maxX = x;
            rightPoint[ZERO] = x;
            rightPoint[1] = y;
        }

        if (y < minY) {
            minY = y;
            topPoint[ZERO] = x;
            topPoint[1] = y;
        }

        if (y > maxY) {
            maxY = y;
            bottomPoint[ZERO] = x;
            bottomPoint[1] = y;
        }

        previous = direction;
    } while (x != initialX || y != initialY);

    Path path(initialX, -initialY, directions);
    path.setMaxX(maxX);
    path.setMinX(minX);
    path.setMaxY(maxY);
    path.setMinY(minY);
    path.setShapeWidth(maxX - minX);
    path.setTopPoint(topPoint);
    path.setBottomPoint(bottomPoint);
    path.setLeftPoint(leftPoint);
    path.setRightPoint(rightPoint);
    return path;
}

int MarchingSquares::value(int &xCoordinate, int &yCoordinate) {
    int sum = ZERO;
    int xCoordinateShifted = xCoordinate + 1;
    int yCoordinateShifted = yCoordinate + 1;
    if (isSet(xCoordinate, yCoordinate)) sum |= 1;
    if (isSet(xCoordinateShifted, yCoordinate)) sum |= 2;
    if (isSet(xCoordinate, yCoordinateShifted)) sum |= 4;
    if (isSet(xCoordinateShifted, yCoordinateShifted)) sum |= 8;
    return sum;
}

bool MarchingSquares::isSet(int &xCoordinate, int &yCoordinate) {
    return !(xCoordinate <= ZERO || xCoordinate > width || yCoordinate <= ZERO || yCoordinate > height)
           && data[(yCoordinate - 1) * width + (xCoordinate - 1)] != ZERO;
}

void MarchingSquares::init(int &_width, int &_height, vector<int> &_data) {
    width = _width;
    height = _height;
    data = _data;
}
