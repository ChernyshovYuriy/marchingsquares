#include <utility>

#include "path.h"

Path::Path(int _originX, int _originY, vector<Direction> _directions) : originX(_originX),
                                                                        originY(_originY),
                                                                        directions(std::move(_directions)) {

    int endX = originX;
    int endY = originY;
    int diagonals = 0;

    for (vector<Direction>::size_type i = 0; i != directions.size(); i++) {
        Direction direction = directions[i];
        endX += direction.screenX;
        endY += direction.screenY;
        if (direction.screenX != 0 && direction.screenY != 0) {
            diagonals++;
        }
    }

    terminalX = endX;
    terminalY = endY;

    length = (int) (directions.size() + diagonals * ADJ_LEN);
}

vector<Direction> Path::getDirections() {
    return directions;
}

void Path::setMaxX(int value) {
    maxX = value;
}

void Path::setMinX(int value) {
    minX = value;
}

void Path::setMaxY(int value) {
    maxY = value;
}

void Path::setMinY(int value) {
    minY = value;
}

void Path::setShapeWidth(int value) {
    shapeWidth = value;
}

int Path::getShapeWidth() {
    return shapeWidth;
}

void Path::setTopPoint(const int value[]) {
    topPoint[0] = value[0];
    topPoint[1] = value[1];
}

void Path::setBottomPoint(const int value[]) {
    bottomPoint[0] = value[0];
    bottomPoint[1] = value[1];
}

void Path::setLeftPoint(const int value[]) {
    leftPoint[0] = value[0];
    leftPoint[1] = value[1];
}

void Path::setRightPoint(const int value[]) {
    rightPoint[0] = value[0];
    rightPoint[1] = value[1];
}

int Path::getShapeHeight() {
    return (maxY - minY);
}

int Path::getLength() {
    return length;
}

int Path::getOriginX() {
    return originX;
}

int Path::getOriginY() {
    return originY;
}

int Path::getMinX() {
    return minX;
}

int Path::getMaxX() {
    return maxX;
}

int Path::getMinY() {
    return minY;
}

int Path::getMaxY() {
    return maxY;
}

Path::~Path() {
    directions.clear();
}
