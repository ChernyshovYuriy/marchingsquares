#ifndef DRIVER_ASSISTANT_DIRECTION_H
#define DRIVER_ASSISTANT_DIRECTION_H

#include "constants.h"

static constexpr const int TYPE_DEFAULT = 0;
static constexpr const int TYPE_N = 1;
static constexpr const int TYPE_S = 2;
static constexpr const int TYPE_W = 3;
static constexpr const int TYPE_E = 4;

class Direction {

public:

    Direction();
    void setNorth();
    void setSouth();
    void setWest();
    void setEast();

    int type;
    int planeX;
    int planeY;
    int screenX;
    int screenY;
};

#endif //DRIVER_ASSISTANT_DIRECTION_H
