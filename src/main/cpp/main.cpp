#include <iostream>
#include <chrono>
#include "bitmap/bitmap_image.hpp"
#include "marching_squares.h"

using namespace std;
using namespace std::chrono;

void thresholdPathList(int &imgWidth, int &imgHeight, vector<Path> &pathVector);

void drawOutputImage(int &width, int &height, vector<Path> &data);

int main(int argc, char *argv[]) {

    int width = 0;
    int height = 0;
    vector<int> data;

    // A simple way to separate two logic of data reading: from file and from bitmap image

    const bool isReadFromTextFile = false;
    const bool isReadFromBitmapImage = !isReadFromTextFile;

    if (isReadFromTextFile) {
        string strValue;
        int counter = 0;
        ifstream infile("src/main/assets/data.txt");
        while (getline(infile, strValue)) {
            switch (counter++) {
                case 0:
                    width = atoi(strValue.c_str());
                    break;
                case 1:
                    height = atoi(strValue.c_str());
                    break;
                default:
                    if (data.size() == 0) {
                        data.resize((unsigned long) (width * height));
                    }
                    if (strValue == "1") {
                        data[counter - 3] = 1;
                    } else {
                        data[counter - 3] = 0;
                    }
                    break;
            }

        }
        infile.close();
    } else if (isReadFromBitmapImage) {
        static constexpr const char * const file_path = "src/main/assets/input_image.bmp";
        BitmapImage image(file_path);

        height = image.height();
        width = image.width();
        data.resize(static_cast<unsigned long>(width * height));

        rgb_t colour{};
        int index = 0;

        for (size_t y = ZERO; y < height; ++y) {
            for (size_t x = ZERO; x < width; ++x) {
                image.get_pixel((const unsigned int) x, (const unsigned int) y, colour);
                if (colour.red > ZERO) {
                    data[index] = 1;
                } else {
                    data[index] = ZERO;
                }
                index++;
            }
        }
    }

    cout << "W:" << width << endl;
    cout << "H:" << height << endl;
    cout << "Size:" << width * height << endl;

    MarchingSquares marchingSquares;
    vector<Path> pathVector;
    double avgTime = 0;
    int numberOfTests = 10;

    for (int var = ZERO; var < numberOfTests; ++var) {

        high_resolution_clock::time_point start = high_resolution_clock::now();

        marchingSquares.init(width, height, data);
        pathVector = marchingSquares.identifyPerimeter();

        thresholdPathList(width, height, pathVector);

        high_resolution_clock::time_point stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start).count();
        avgTime += duration;

        cout << "time: " << duration << " ms\tsize:" << pathVector.size() << endl;

        drawOutputImage(width, height, pathVector);
    }

    cout << "avg time: " << (avgTime / numberOfTests) << " ms" << endl;

    return 0;
}

void thresholdPathList(int &imgWidth, int &imgHeight, vector<Path> &pathVector) {
    // Iterator index.
    int i;
    // Iterator index.
    int j;
    // Width threshold to determine small shape (shape to be excluded later).
    int minWidth = imgWidth * 3 / 100;
    // Height threshold to determine small shape (shape to be excluded later).
    int minHeight = imgHeight * 3 / 100;

    // Eliminate small objects
    for (j = 0; j < pathVector.size(); j++) {
        Path path = pathVector[j];
        if (path.getShapeWidth() < minWidth || path.getShapeHeight() < minHeight) {
            pathVector.erase(pathVector.begin() + j);
        }
    }

    // Eliminate nested shapes
    for (j = 0; j < pathVector.size(); j++) {

        Path path = pathVector[j];
        for (i = j + 1; i < pathVector.size(); i++) {

            Path pathInner = pathVector[i];
            if (path.getMinX() <= pathInner.getMinX()
                && path.getMinY() <= pathInner.getMinY()
                && path.getMaxX() >= pathInner.getMaxX()
                && path.getMaxY() >= pathInner.getMaxY()) {
                pathVector.erase(pathVector.begin() + i);
            }
        }
    }
}

void drawOutputImage(int &width, int &height, vector<Path> &data) {
    int lastX;
    int lastY;
    BitmapImage image2((const unsigned int) width, (const unsigned int) height);
    image2.set_all_channels(ZERO, ZERO, ZERO);
    image_drawer draw(image2);

    for (int i = ZERO; i < data.size(); i++) {
        Path path = data[i];

        lastX = path.getOriginX();
        lastY = -path.getOriginY();

        for (int j = ZERO; j < path.getLength(); j++) {

            Direction direction = path.getDirections()[j];

            lastX += direction.screenX;
            lastY += direction.screenY;

            draw.pen_width(1);
            draw.pen_color(ZERO, ZERO, 255);
            draw.rectangle(lastX, lastY, lastX + 1, lastY + 1);
        }
    }

    image2.save_image("src/main/assets/output_image.bmp");
}
