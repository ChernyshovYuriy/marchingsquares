#ifndef DRIVER_ASSISTANT_PATH_H
#define DRIVER_ASSISTANT_PATH_H

#include "direction.h"
#include "constants.h"
#include <vector>
#include <iostream>
#include <math.h>

using namespace std;

static double ADJ_LEN = sqrt(2.0) / 2.0 - 1;

class Path {

public:

    Path(int _originX, int _originY, vector<Direction> _directions);

    ~Path();

    vector<Direction> getDirections();

    void setMaxX(int value);

    void setMinX(int value);

    void setMaxY(int value);

    void setMinY(int value);

    void setShapeWidth(int value);

    void setTopPoint(const int value[]);

    void setBottomPoint(const int value[]);

    void setLeftPoint(const int value[]);

    void setRightPoint(const int value[]);

    int getShapeWidth();

    int getShapeHeight();

    int getLength();

    int getOriginX();

    int getOriginY();

    int getMinX();

    int getMaxX();

    int getMinY();

    int getMaxY();

private:
    vector<Direction> directions;
    int length;
    int originX;
    int originY;
    int terminalX;
    int terminalY;
    int shapeWidth{};
    int minX{};
    int maxX{};
    int minY{};
    int maxY{};
    int topPoint[2] = {ZERO, ZERO};
    int bottomPoint[2] = {ZERO, ZERO};
    int leftPoint[2] = {ZERO, ZERO};
    int rightPoint[2] = {ZERO, ZERO};
};

#endif //DRIVER_ASSISTANT_PATH_H
